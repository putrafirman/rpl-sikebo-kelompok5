<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model {
    
    public $timestamps = false;
    protected $fillable = [
        'comment_id', 'user_id',
    ];

}
