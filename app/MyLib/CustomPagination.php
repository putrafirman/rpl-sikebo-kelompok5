<?php

namespace App\MyLib;

use Landish\Pagination\Materialize;

class CustomPagination extends Materialize {

    protected $previousButtonText = '<i class="mdi mdi-chevron-left"></i>';
    protected $nextButtonText = '<i class="mdi mdi-chevron-right"></i>';

}
