<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model {
    
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'user_id_foll',
    ];

}
