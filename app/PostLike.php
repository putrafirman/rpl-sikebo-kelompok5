<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model {
    
    public $timestamps = false;
    protected $fillable = [
        'post_id', 'user_id',
    ];
    
    public function post(){
        return $this->belongsTo('App\Post');
    }

}
