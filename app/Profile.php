<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $primaryKey = 'user_id';
    protected $fillable   = [
        'name', 'gender', 'birthdate', 'about', 'created_at', 'updated_at',
    ];
}
