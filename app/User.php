<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    protected $fillable = [
        'username', 'password', 'role', 'email',
    ];
    protected $hidden = [
        'password',
    ];
    public $timestamps = false;

    public function profile() {
        return $this->hasOne('App\Profile');
    }

    public function following() {
        return $this->hasMany('App\Follow');
    }
    
    public function followed() {
        return $this->hasMany('App\Follow','user_id_foll');
    }

    public function post() {
        return $this->hasMany('App\Post');
    }

    public function postLiked() {
        return $this->hasMany('App\PostLike');
    }

    public function comment() {
        return $this->hasMany('App\Comment');
    }

    public function commentLiked() {
        return $this->hasMany('App\CommentLike');
    }

    public function reported() {
        return $this->hasMany('App\Report');
    }
    
    public function adminMessage() {
        return $this->hasMany('App\AdminMessage');
    }

}
