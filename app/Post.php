<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $fillable = [
        'user_id', 'caption', 'created_at', 'updated_at','title','description','year','youtube'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function liked() {
        return $this->hasMany('App\PostLike');
    }

    public function comment() {
        return $this->hasMany('App\Comment');
    }

    public function reported() {
        return $this->hasMany('App\Report');
    }

}
