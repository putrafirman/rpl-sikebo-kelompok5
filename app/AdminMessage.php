<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminMessage extends Model {

    protected $fillable = [
        'admin_id', 'user_id', 'message', 'created_at', 'updated_at',
    ];

    public function receiver() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function sender() {
        return $this->belongsTo('App\User', 'admin_id');
    }

}
