<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $fillable = [
        'post_id', 'user_id', 'comment', 'created_at', 'updated_at',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function liked() {
        return $this->hasMany('App\CommentLike');
    }
    
    public function post(){
        return $this->belongsTo('App\Post');
    }

}
