<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Route::group(['middleware' => 'web'], function () {   
//     Route::get('/index', function () {
//          return view('indexhome');
//      });
// });


Route::get('/index', 'GuestController@index');

Route::auth();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('/latestpost', 'HomeController@showLatestPost');

Route::post('post', ['middleware' => 'auth', 'uses' => 'FileInputController@post']);
Route::post('post/{id}/editCaption', ['middleware' => 'auth', 'uses' => 'PostController@updateCaption']);
Route::post('post/{id}/comment', ['middleware' => 'auth', 'uses' => 'CommentController@comment']);

Route::get('post/{id}', ['middleware' => 'auth', 'uses' => 'PostController@index']);
Route::get('post/{id}/delete', ['middleware' => 'auth', 'uses' => 'PostController@delete']);
Route::get('post/like/{id}', ['middleware' => 'auth', 'uses' => 'PostController@like']);
Route::get('post/report/{id}', ['middleware' => 'auth', 'uses' => 'PostController@report']);

Route::post('comment/edit/{comment_id}', ['middleware' => 'auth', 'uses' => 'CommentController@editComment']);
Route::get('comment/like/{id}', ['middleware' => 'auth', 'uses' => 'CommentController@like']);
Route::get('comment/delete/{comment_id}', ['middleware' => 'auth', 'uses' => 'CommentController@delete']);

Route::get('user/{username}', ['middleware' => 'auth', 'uses' => 'UserController@index']);
Route::get('user/id/{id}', ['middleware' => 'auth', 'uses' => 'UserController@byId']);
Route::get('user/follow/{id}', ['middleware' => 'auth', 'uses' => 'UserController@Follow']);

Route::post('settings/updateProfile', ['middleware' => 'auth', 'uses' => 'UserController@updateProfile']);
Route::post('settings/updatePassword', ['middleware' => 'auth', 'uses' => 'UserController@updatePassword']);
Route::post('settings/review', ['middleware' => 'auth', 'uses' => 'FileInputController@reviewAvatar']);
Route::post('settings/uploadAvatar', ['middleware' => 'auth', 'uses' => 'FileInputController@uploadAvatar']);
Route::get('settings', ['middleware' => 'auth', 'uses' => 'SettingController@index']);

//Admin only
Route::get('admin/login', function() {
    if (!Auth::check()) {
        return view('admin.login');
    }
});

Route::post('admin/login', 'Admin\AdminController@login');

Route::get('admin', ['middleware' => 'admin', 'uses' => 'Admin\AdminController@index']);
Route::get('admin/{table}', ['middleware' => 'admin', 'uses' => 'Admin\AdminController@showCRUD']);

Route::post('admin/user/create', ['middleware' => 'admin', 'uses' => 'Admin\UserController@create']);
Route::post('admin/user/edit/{id}', ['middleware' => 'admin', 'uses' => 'Admin\UserController@edit']);
Route::get('admin/user/show/{id?}', ['middleware' => 'admin', 'uses' => 'Admin\UserController@show']);
Route::get('admin/user/delete/{id}', ['middleware' => 'admin', 'uses' => 'Admin\UserController@delete']);

Route::post('admin/post/create', ['middleware' => 'admin', 'uses' => 'Admin\PostController@create']);
Route::post('admin/post/edit/{id}', ['middleware' => 'admin', 'uses' => 'Admin\PostController@edit']);
Route::get('admin/post/show/{id?}', ['middleware' => 'admin', 'uses' => 'Admin\PostController@show']);
Route::get('admin/post/delete/{id}', ['middleware' => 'admin', 'uses' => 'Admin\PostController@delete']);

Route::post('admin/comment/create', ['middleware' => 'admin', 'uses' => 'Admin\CommentController@create']);
Route::post('admin/comment/edit/{id}', ['middleware' => 'admin', 'uses' => 'Admin\CommentController@edit']);
Route::get('admin/comment/show/{id?}', ['middleware' => 'admin', 'uses' => 'Admin\CommentController@show']);
Route::get('admin/comment/delete/{id}', ['middleware' => 'admin', 'uses' => 'Admin\CommentController@delete']);

Route::post('message/send', ['middleware' => 'admin', 'uses' => 'Admin\AdminMessageController@send']);
Route::get('message/delete/{id}', ['middleware' => 'admin', 'uses' => 'Admin\AdminMessageController@delete']);
