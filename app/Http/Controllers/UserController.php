<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use App\Follow;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Hash;

class UserController extends Controller {

    public function index($username) {
        $user = User::where('username', $username)->first();
        if ($user == null) {
            return view('errors/404');
        } else {
            $userPosts = Post::where('user_id', $user->id)->orderBy('id', 'desc');
            $userLikedPosts = Post::whereHas('liked', function($query) use ($user) {
                        $query->where('user_id', $user->id);
                    })->orderBy('id', 'desc');

            $data['user'] = $user;
            $data['size'] = 'med';

            $data['userPosts'] = $userPosts->paginate(10, ['*'], 'pagea');
            $data['totUserPosts'] = Post::where('user_id', $user->id)->orderBy('id', 'desc')->count();

            $data['userLikedPosts'] = $userLikedPosts->paginate(10, ['*'], 'pageb');
            $data['totUserLikedPosts'] = Post::whereHas('liked', function($query) use ($user) {
                        $query->where('user_id', $user->id);
                    })->orderBy('id', 'desc')->count();

            return view('pages/user', $data);
        }
    }

    public function byId($id) {
        $username = User::find($id)->username;
        return redirect('user/' . $username);
    }

    public function updateProfile(Request $request) {
        $rules = [
            'name' => 'required|max:40',
            'email' => 'required|email|max:40',
            'gender' => 'required',
            'birthdate' => 'required|date',
            'about' => 'max:140',
        ];
        if (Auth::user()->email != $request->input('email')) {
            $rules['email'] = 'required|email|max:40|unique:users';
        }

        $this->validate($request, $rules);

        $user = User::find(Auth::user()->id);
        $profile = $user->profile;
        $user->email = $request->input('email');
        $profile->name = $request->input('name');
        $profile->gender = $request->input('gender');
        $profile->birthdate = $request->input('birthdate');
        $profile->about = $request->input('about');
        $profile->save();
        $user->save();
        return redirect(url('settings'))->with('status', 'Berhasil ubah profile!');
    }

    public function updatePassword(Request $request) {
        $this->validate($request, [
            'oldPassword' => 'required|max:24',
            'password' => 'required|max:24|confirmed',
        ]);
        if (Hash::check($request->input('oldPassword'), Auth::user()->password)) {
            Auth::user()->password = bcrypt($request->input('password'));
            Auth::user()->save();
            return redirect(url('settings'))->with('status', 'Berhasil ubah password!');
        } else {
            return redirect(url('settings'))->with('error', 'Password yang dimasukkan salah');
        }
    }

    public function follow($id) {
        $followed = Auth::user()->following()->where('user_id_foll', $id)->get();
        if ($followed->count() > 0) {
            $followed->first()->delete();
            $follow = 'Follow';
        } else {
            Follow::create([
                'user_id' => Auth::user()->id,
                'user_id_foll' => $id,
            ]);
            $follow = 'Unfollow';
        }
        echo '<a href="#!" class="secondary-content waves-effect waves-green btn-flat" onclick="follow(' . $id . ')">';
        echo $follow . '</a>';
    }

}
