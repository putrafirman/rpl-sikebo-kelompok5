<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    public function delete($id) {
        User::find($id)->delete();
        return redirect('admin/user')->with('status', 'Berhasil delete user!');
    }

    public function show($id = null) {
        $create = ($id == null);
        $active = '';
        if (!$create) {
            $user = User::find($id);
            $active = 'active';
        }
        ?>
        <form action="<?php echo url('admin/user/' . (($create) ? 'create' : 'edit/' . $id)) ?>" method="post">
            <?php echo csrf_field() ?>
            <div class="modal-content">
                <?php if ($create) { ?>
                    <h4>New User</h4>
                    <div class="input-field">
                        <input type="text" name="username"/>
                        <label for="username">Username</label>
                    </div>
                    <div class="input-field">
                        <input type="password" name="password"/>
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field">
                        <input type="password" name="password_confirmation"/>
                        <label for="password_confirmation">Konfirmasi Password</label>
                    </div>
                <?php } else { ?>
                    <h4>Edit User <?php echo $id ?></h4>
                <?php } ?>
                <div class="input-field">
                    <select name="role">
                        <option value="user" <?php if (!$create && $user->role == 'user') echo 'selected' ?> >
                            User
                        </option>
                        <option value="admin" <?php if (!$create && $user->role == 'admin') echo 'selected' ?> >
                            Admin
                        </option>
                    </select>
                    <label>Role</label>
                </div>
                <div class="input-field">
                    <input type="text" name="email" value="<?php if (!$create) echo $user->email ?>" />
                    <label class="<?php echo $active ?>" for="email">Email</label>
                </div>
                <div class="input-field">
                    <input type="text" name="name" value="<?php if (!$create) echo $user->profile->name ?>" />
                    <label class="<?php echo $active ?>" for="name">Nama</label>
                </div>
                <div class="input-field">
                    <select name="gender">
                        <option value="1" <?php if (!$create && $user->profile->gender) echo 'selected' ?> >
                            Laki - Laki
                        </option>
                        <option value="0" <?php if (!$create && !$user->profile->gender) echo 'selected' ?> >
                            Perempuan
                        </option>
                    </select>
                    <label>Jenis Kelamin</label>
                </div>
                <div class="input-field">
                    <input type="date" name="birthdate" class="datepicker" value="<?php if (!$create) echo $user->profile->birthdate ?>" >
                    <label class="<?php echo $active ?>" for="birthdate">Tanggal Lahir</label>
                </div>
                <div class="input-field">
                    <div class="input-field">
                        <textarea name="about" class="materialize-textarea" length="140"><?php if (!$create) echo $user->profile->about ?></textarea>
                        <label class="<?php echo $active ?>" for="about">About Me</label>
                    </div>
                </div>
                <button class="green btn waves-effect waves-light" type="submit" name="submit">
                    Submit
                </button>
            </div>
        </form>
        <?php
    }

    public function edit($id, Request $request) {
        $user = User::find($id);

        $rules = [
            'role' => 'required|max:5',
            'email' => 'required|email|max:40',
            'name' => 'required|max:40',
            'gender' => 'required|max:1',
            'birthdate' => 'required',
            'about' => 'max:140',
        ];
        if ($user->email != $request->input('email')) {
            $rules['email'] = 'required|unique:users';
        }

        $this->validate($request, $rules);

        $user->role = $request->input('role');
        $user->email = $request->input('email');
        $user->profile->name = $request->input('name');
        $user->profile->gender = $request->input('gender');
        $user->profile->birthdate = $request->input('birthdate');
        $user->profile->about = $request->input('about');
        $user->profile->save();
        $user->save();
        return redirect('admin/user')->with('status', "Berhasil edit user id $id!");
    }

    public function create(Request $request) {
        $this->validate($request, [
            'username' => 'required|max:24|unique:users',
            'password' => 'required|max:24|confirmed',
            'role' => 'required|max:5',
            'email' => 'required|email|max:40|unique:users',
            'name' => 'required|max:40',
            'gender' => 'required|max:1',
            'birthdate' => 'required',
            'about' => 'max:140',
        ]);

        $user = User::create([
                    'username' => $request->input('username'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'role' => $request->input('role'),
        ]);
        $user->profile()->create([
            'name' => $request->input('name'),
            'gender' => $request->input('gender'),
            'birthdate' => $request->input('birthdate'),
            'about' => $request->input('about'),
        ]);
        return redirect('admin/user')->with('status', "Berhasil membuat user!");
    }

}
