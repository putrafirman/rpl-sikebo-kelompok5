<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller {

    public function delete($id) {
        Post::find($id)->delete();
        return redirect('admin/post')->with('status', 'Berhasil delete post!');
    }

    public function show($id = null) {
        $create = ($id == null);
        $active = '';
        if (!$create) {
            $post = Post::find($id);
            $active = 'active';
        }
        ?>
        <form enctype="multipart/form-data" action="<?php echo url('admin/post/' . (($create) ? 'create' : 'edit/' . $id)) ?>" method="post">
            <?php echo csrf_field() ?>
            <div class="modal-content">
                <?php if ($create) { ?>
                    <h4>New Post</h4>
                    <div class="file-field input-field">
                        <div class="green btn waves-effect waves-light" id="inFile">
                            <span>File</span>
                            <input name="image" type="file" accept=".jpg, .png">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                <?php } else { ?>
                    <h4>Edit Post <?php echo $id ?></h4>
                <?php } ?>
                <div class="input-field">
                    <textarea name="caption" id="caption" class="materialize-textarea" length="140"><?php if (!$create) echo $post->caption ?></textarea>
                    <label for="caption" class="<?php echo $active?>">Caption</label>
                </div>
            </div>
            <div class="modal-footer">
                <button class="green btn waves-effect waves-light" id="submit">
                    Submit
                </button>
            </div>
        </form>
        <?php
    }

    public function edit($id, Request $request) {
        $this->validate($request, [
            'caption' => 'required|max:140',
        ]);
        $post = Post::find($id);
        $post->caption = $request->input('caption');
        $post->save();
        return redirect('admin/post')->with('status', "Berhasil edit post id $id!");
    }

    public function create(Request $request) {
        $this->validate($request, [
            'caption' => 'required|max:140',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);
        $post = Post::create([
                    'user_id' => Auth::user()->id,
                    'caption' => $request->input('caption'),
        ]);
        $file = $request->file('image');
        $file->move(public_path('img/post/'), $post->id . '.jpg');
        return redirect('admin/post')->with('status', 'Berhasil membuat post!');
    }

}
