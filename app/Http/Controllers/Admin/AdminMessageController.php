<?php

namespace App\Http\Controllers\Admin;

use App\AdminMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdminMessageController extends Controller {

    public function send(Request $request) {
        $this->validate($request, [
            'message' => 'required|max:140',
            'receiver' => 'required',
        ]);
        $post = AdminMessage::create([
                    'admin_id' => Auth::user()->id,
                    'user_id' => $request->input('receiver'),
                    'message' => $request->input('message'),
        ]);
        return redirect('admin')->with('status', 'Berhasil mengirimkan pesan!');
        ;
    }

    public function delete($id) {
        AdminMessage::find($id)->delete();
        return redirect('admin')->with('status', 'Berhasil delete pesan!');
    }

}
