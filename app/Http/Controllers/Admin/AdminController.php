<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\AdminMessage;
use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdminController extends Controller {

    public function index() {
        $data['adminMessages'] = AdminMessage::all();
        $users = User::all();
        $posts = Post::all();
        $comments = Comment::all();

        if ($users->count() > 0) {
            $data['mostFollowedUser'] = $users->first();
            foreach ($users as $user) {
                if ($user->followed->count() > $data['mostFollowedUser']->followed->count()) {
                    $data['mostFollowedUser'] = $user;
                }
            }
        }

        if ($users->count() > 0) {
            $data['topPoster'] = $users->first();
            foreach ($users as $user) {
                if ($user->post->count() > $data['topPoster']->post->count()) {
                    $data['topPoster'] = $user;
                }
            }
        }

        if ($users->count() > 0) {
            $data['topCommenter'] = $users->first();
            foreach ($users as $user) {
                if ($user->comment->count() > $data['topCommenter']->comment->count()) {
                    $data['topCommenter'] = $user;
                }
            }
        }

        if ($posts->count() > 0) {
            $data['mostLikedPost'] = $posts->first();
            foreach ($posts as $post) {
                if ($post->liked->count() > $data['mostLikedPost']->liked->count()) {
                    $data['mostLikedPost'] = $post;
                }
            }
        }

        if ($posts->count() > 0) {
            $data['mostReportedPost'] = $posts->first();
            foreach ($posts as $post) {
                if ($post->reported->count() > $data['mostReportedPost']->reported->count()) {
                    $data['mostReportedPost'] = $post;
                }
            }
        }

        if ($comments->count() > 0) {
            $data['mostLikedComment'] = $comments->first();
            foreach ($comments as $comment) {
                if ($comment->liked->count() > $data['mostLikedComment']->liked->count()) {
                    $data['mostLikedComment'] = $comment;
                }
            }
        }

        $data['users'] = $users;
        $data['posts'] = $posts;
        $data['comments'] = $comments;

        return view('admin/home', $data);
    }

    public function login(Request $request) {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $userdata = array(
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'role' => 'admin',
        );

        if (Auth::attempt($userdata)) {
            return redirect('admin');
        } else {
            return redirect('admin/login')->withInput()->with('gagal', 'Username atau Password yang dimasukkan salah, atau anda bukan admin');
        }
    }

    public function showCRUD($table) {
        if ($table == 'user') {
            $data['users'] = User::all();
        } elseif ($table == 'post') {
            $data['posts'] = Post::all();
        } elseif ($table == 'comment') {
            $data['comments'] = Comment::all();
        } else {
            return view('errors/404');
        }
        return view('admin/crud' . $table, $data);
    }

}
