<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller {

    public function delete($id) {
        Comment::find($id)->delete();
        return redirect('admin/comment')->with('status', 'Berhasil delete comment!');
    }

    public function show($id = null) {
        $create = ($id == null);
        $active = '';
        if ($create) {
            $posts = Post::all();
        } else {
            $comment = Comment::find($id);
            $active = 'active';
        }
        ?>
        <form enctype="multipart/form-data" action="<?php echo url('admin/comment/' . (($create) ? 'create' : 'edit/' . $id)) ?>" method="post">
            <?php echo csrf_field() ?>
            <div class="modal-content">
                <?php if ($create) { ?>
                    <h4>New Comment</h4>
                    <div class="input-field">
                        <select name="post_id">
                            <?php foreach ($posts as $post) { ?>
                                <option value="<?php echo $post->id ?>"><?php echo $post->id ?></option>
                            <?php } ?>
                        </select>
                        <label for="post_id">Post_id</label>
                    </div>
                <?php } else { ?>
                    <h4>Edit Comment <?php echo $id ?></h4>
                <?php } ?>
                <div class="input-field">
                    <textarea name="comment" id="comment" class="materialize-textarea" length="140"><?php if (!$create) echo $comment->comment ?></textarea>
                    <label for="comment" class="<?php echo $active ?>">Comment</label>
                </div>
            </div>
            <div class="modal-footer">
                <button class="green btn waves-effect waves-light" id="submit">
                    Submit
                </button>
            </div>
        </form>
        <?php
    }

    public function edit($id, Request $request) {
        $this->validate($request, [
            'comment' => 'required|max:140',
        ]);
        $comment = Comment::find($id);
        $comment->comment = $request->input('comment');
        $comment->save();
        return redirect('admin/comment')->with('status', "Berhasil edit comment id $id!");
    }

    public function create(Request $request) {
        $this->validate($request, [
            'comment' => 'required|max:140',
        ]);
        Comment::create([
            'user_id' => Auth::user()->id,
            'post_id' => $request->input('post_id'),
            'comment' => $request->input('comment'),
        ]);
        return redirect('admin/comment')->with('status', 'Berhasil membuat comment!');
    }

}
