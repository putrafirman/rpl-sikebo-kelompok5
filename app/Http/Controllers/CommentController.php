<?php

namespace App\Http\Controllers;

use App\Comment;
use App\CommentLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller {

    public function comment(Request $request, $id) {
        $this->validate($request, [
            'comment' => 'required|max:140',
        ]);
        $comment = Comment::create([
                    'post_id' => $id,
                    'user_id' => Auth::user()->id,
                    'comment' => $request->input('comment'),
        ]);
        return redirect('/post/' . $id)->with('status', 'Berhasil comment!');
    }

    public function editComment($id, Request $request) {
        $this->validate($request, [
            'comment' => 'required|max:140',
        ]);
        $comment = Comment::find($id);
        $comment->comment = $request->input('comment');
        $comment->save();
        $post_id = $comment->post_id;
        return redirect(url('post/' . $post_id))->with('status', 'Berhasil edit comment!');
    }

    public function delete($id) {
        $comment = Comment::find($id);
        if (Auth::user()->id == $comment->user_id) {
            if ($comment == null) {
                return redirect('404');
            }
            $post_id = $comment->post_id;
            $comment->delete();
            $data['status'] = 'Berhasil delete comment!';
        } else {
            $data['status'] = 'Gagal delete comment!';
        }
        return redirect(url('post/' . $post_id))->with($data);
    }

    public function like($id) {
        $postLiked = Auth::user()->commentLiked()->where('comment_id', $id)->get();
        if ($postLiked->count() > 0) {
            $postLiked->first()->delete();
            $likeColor = 'grey';
        } else {
            CommentLike::create([
                'comment_id' => $id,
                'user_id' => Auth::user()->id,
            ]);
            $likeColor = 'red';
        }
        echo '<a href="#!" class="secondary-content ' . $likeColor . '-text" onclick="likeComment(' . $id . ')" style="font-size:18px">';
        echo CommentLike::where('comment_id', $id)->get()->count() . ' <i class="mdi mdi-heart"></i></a>';
    }

}
