<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\MyLib\ImageManipulator;

class FileInputController extends Controller {

    public function post(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:100',
            'description' => 'required|max:255',
            'youtube' => 'required|max:255',
            'year' => 'required|max:5',
            'caption' => 'required|max:255',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);
        $post = Post::create([
                    'user_id' => Auth::user()->id,
                    'caption' => $request->input('caption'),
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'year' => $request->input('year'),
                    'youtube' => $request->input('youtube'),
        ]);
        $file = $request->file('image');
        $file->move(public_path('img/post/'), $post->id . '.jpg');
        return redirect('/home')->with('status', 'Berhasil post!');
    }

    public function reviewAvatar(Request $request) {
        $this->validate($request, [
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);
        $path = public_path('img/tmp/');
        $filename = Auth::user()->id . '.jpg';
        if ($request->file('image')->move($path, $filename)) {
            ?>
            <div class="col s6 push-s3">
                <p>Silahkan crop foto agar dapat digunakan sebagai avatar</p>
                <img src="<?php echo asset('img/tmp') . '/' . $filename ?>"  class="responsive-img" id="thumbnail"/>
                <form id="uploadAvatar" action="settings/uploadAvatar" method="post">
                    <?php echo csrf_field() ?>
                    <input type="hidden" id="x1" name="x1">
                    <input type="hidden" id="y1" name="y1">
                    <input type="hidden" id="x2" name="x2">
                    <input type="hidden" id="y2" name="y2">
                    <input type="hidden" id="fullw" name="fullw">
                    <input type="hidden" id="fullh" name="fullh">
                    <button class="btn waves-effect waves-light green">Done</button>
                </form>
            </div>
            <script>avatarReady();</script>
            <?php
        }
    }

    public function uploadAvatar(Request $request) {
        $this->validate($request, [
            'fullw' => 'required'
        ]);
        $filepath = public_path('img/tmp/' . Auth::user()->id . '.jpg');
        $manipulator = new ImageManipulator($filepath);

        // Dimensi asli
        $old = getimagesize($filepath);
        $oldWidth = $old[0];
        $oldHeight = $old[1];

        // Dimensi saat ditampilkan
        $wRatio = $oldWidth / $request->input('fullw');
        $hRatio = $oldHeight / $request->input('fullh');
        $x1 = $request->input('x1') * $wRatio;
        $y1 = $request->input('y1') * $hRatio;
        $x2 = $request->input('x2') * $wRatio;
        $y2 = $request->input('y2') * $hRatio;

        $manipulator->crop($x1, $y1, $x2, $y2);
        $manipulator->save(public_path('img/user/' . Auth::user()->id . '.jpg'));
        return redirect('settings', '302')->with('status', 'Berhasil ubah avatar!');
    }

}
