<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Hash;

class SettingController extends Controller {

    public function index() {
        return view('pages/settings');
    }

}
