<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data['size'] = 'home';

        $data['posts'] = Post::whereHas('user.followed', function ($query) {
                    $query->where('user_id', Auth::user()->id);
                })->orWhereHas('user', function($query) {
                    $query->where('user_id', Auth::user()->id);
                })->orderBy('id', 'desc')->paginate(10);

        return view('home', $data);
    }

    public function showLatestPost() {
        $data['size'] = 'home';
        $post = Post::take(1)->orderBy('id', 'desc')->get()[0];
        $doPost = false;

        foreach (Auth::user()->following as $following) {
            if ($following->user_id_foll == $post->user_id) {
                $doPost = true;
            }
        }
        if ($doPost) {
            $data['post'] = $post;
            return view('shared.showPost', $data);
        }
    }

}
