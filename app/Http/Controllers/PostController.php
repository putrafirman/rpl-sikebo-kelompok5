<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostLike;
use App\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller {

    public function index($id) {
        $post = Post::find($id);
        if ($post === null) {
            return view('errors/404');
        } else {
            $data['size'] = 'large';
            $data['post'] = $post;
            $data['comments'] = $post->comment;
            return view('pages/post', $data);
        }
    }

    public function updateCaption($id, Request $request) {
        $this->validate($request, [
            'caption' => 'required|max:140',
        ]);
        $post = Post::find($id);
        $post->caption = $request->input('caption');
        $post->save();
        return redirect(url('post/' . $id))->with('status', 'Berhasil edit caption!');
    }

    public function delete($id) {
        $post = Post::find($id);
        if (Auth::user()->id == $post->user_id) {
            $post->delete();
            $data['status'] = 'Berhasil delete post!';
        }else{
            $data['status'] = 'Gagal delete post!';
        }
        return redirect(url('home'))->with($data);
    }

    public function like($id) {
        $postLiked = Auth::user()->postLiked()->where('post_id', $id)->get();
        if ($postLiked->count() > 0) {
            $postLiked->first()->delete();
            $likeColor = 'grey';
        } else {
            PostLike::create([
                'post_id' => $id,
                'user_id' => Auth::user()->id,
            ]);
            $likeColor = 'red';
        }
        echo '<a href="#!" class="right ' . $likeColor . '-text" onclick="like(' . $id . ')" style="font-size:18px">';
        echo PostLike::where('post_id', $id)->get()->count();
        echo ' <i class="mdi mdi-heart"></i></a>';
    }

    public function report($id) {
        $postReported = Auth::user()->reported()->where('post_id', $id)->get();
        if ($postReported->count() > 0) {
            $postReported->first()->delete();
            $reportColor = 'grey';
        } else {
            Report::create([
                'post_id' => $id,
                'user_id' => Auth::user()->id,
            ]);
            $reportColor = 'red';
        }
        echo '<a href="#!" class="right ' . $reportColor . '-text" onclick="report(' . $id . ')" style="font-size:18px">';
        echo Report::where('post_id', $id)->get()->count();
        echo ' <i class="mdi mdi-flag"></i></a>';
    }

}
