<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Collection;
//use App\MyLib\CustomPagination;

class GuestController extends Controller {

    public function __construct() {
        $this->middleware('web');
    }

    public function index() {
        $data['size'] = 'indexhome';

        $data['posts'] = Post::orderBy('id', 'DESC')->take('2')->get();;
        //$data['posts'] = Post::All()->paginate(10);
        return view('indexhome', $data);
    }

    public function showLatestPost() {
    }

}