@extends('layouts.user')

@section('title')
Register
@endsection

@section('content')
<div class="row" style="margin-top: 4em">
    <div class="col s10 push-s1 m6 push-m3 l4 push-l4">
        <div class="card white z-depth-2">
            {{ Form::open(array('url' =>'register', 'class' =>'form-input')) }}
            <div class="card-content black-text">
                <span class="card-title">
                    Register
                </span>
                <div class="divider" style="margin-bottom:0.5em"></div>
                @include('shared.formError')
                <div class="input-field">
                    {{ Form::text('username') }}
                    {{ Form::label('username', 'Username') }}
                </div>
                <div class="input-field">
                    {{ Form::text('email') }}
                    {{ Form::label('email', 'Email') }}
                </div>
                <div class="input-field">
                    {{ Form::password('password') }}
                    {{ Form::label('password', 'Password') }}
                </div>
                <div class="input-field">
                    {{ Form::password('password_confirmation') }}
                    {{ Form::label('password_confirmation', 'Konfirmasi Password') }}
                </div>
                <div class="input-field">
                    {{ Form::text('name') }}
                    {{ Form::label('name', 'Nama') }}
                </div>
                <div class="input-field">
                    <select name="gender">
                        <option value="1">
                            Laki - Laki
                        </option>
                        <option value="0">
                            Perempuan
                        </option>
                    </select>
                    {{ Form::label('gender', 'Jenis Kelamin') }}
                </div>
                <div class="input-field">
                    <input type="date" name="birthdate" class="datepicker"/>
                    {{ Form::label('birthdate', 'Tanggal Lahir') }}
                </div>
            </div>
            <div class="card-action">
                <button class="green btn waves-effect waves-light" type="submit" name="submit">
                    Register
                </button>
                <a class="btn waves-effect waves-light btn-flat right" href="{{ url('/login') }}">
                    Login
                </a>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 2
        });
        $('select').material_select();
    });
</script>
@endsection
