@extends('layouts.user')

@section('title')
{{$post->user->username .' - '.$post->caption}}
@endsection

@section('content')
<div class="row">
    @include('shared.showPost')
</div>
<!-- Box Komentar -->
<div class="row">
    <div class="col s12">
        <div class="card">
            <!-- Form Komentar -->
            <form action="{{url('post/'.$post->id.'/comment')}}" method="post" class="form-input">
                {!! csrf_field() !!}
                <div class="card-content">
                    @include('shared.formError')
                    <div class="input-field" id="txtKomen" style="margin-bottom:1.5em">
                        <textarea name="comment" id="comment" class="materialize-textarea" length="140"></textarea>
                        <label for="comment">Komentar</label>
                    </div>
                    <button class="btn waves-effect waves-light green right">
                        Komen
                    </button>
                    <div class="row"></div>
                </div>
            </form>
            <div class="card-content">
                <div class="divider" style="margin-top: -2em;margin-bottom: 1em"></div>
                <!-- Komentar -->
                <ul class="collection" id="comments">
                    <?php
                    if ($comments->count() > 0) {
                        foreach ($comments as $comment) {
                            $user = $comment->user;
                            $likeColor = 'grey';
                            foreach (Auth::user()->commentLiked as $commentLiked) {
                                if ($commentLiked->comment_id == $comment->id) {
                                    $likeColor = 'red';
                                }
                            }
                            ?>
                            <li class="collection-item avatar">
                                <a href="{{url('user/'.$user->username)}}">
                                    <img src="{{asset('img/user/'.$user->id.'.jpg')}}" alt="" class="circle">
                                    <span class="title">
                                        {{$user->username}}
                                    </span>
                                </a>
                                <div class="comment{{$comment->id}}">
                                    <p>
                                        {{$comment->comment}}
                                    </p>
                                </div>
                                @if($comment->user_id == Auth::user()->id)
                                <a href="#!" class="right" style="font-size:18px" onclick="editComment({{$comment->id}})">
                                    <i class="mdi mdi-pencil right"></i>
                                </a>
                                <a href="#!" class="right" style="font-size:18px" onclick="deleteComment({{$comment->id}})">
                                    <i class="mdi mdi-delete right"></i>
                                </a>
                                @endif
                                <div class="likeComment{{$comment->id}}">
                                    <a href="#!" class="secondary-content {{$likeColor}}-text" onclick="likeComment({{$comment->id}})" style="font-size:18px">
                                        {{$comment->liked->count()}}
                                        <i class="mdi mdi-heart"></i>
                                    </a>
                                </div>
                                <p style="font-size:10px">
                                    {{$comment->created_at}}
                                    @if($comment->created_at != $comment->updated_at)
                                    (Edited {{$comment->updated_at}})
                                    @endif
                                </p>
                            </li>
                            <?php
                        }
                    } else {
                        echo '<p> Tidak ada komentar</p>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
    $('#comment').val('');
    $('#comment').trigger('autoresize');
    $('textarea#comment').characterCounter();
    });
</script>
@endsection
