<?php
$profileOpen = false;
$avatarOpen = false;
$passwordOpen = false;
if ($errors->first('name') ||
        $errors->first('email') ||
        $errors->first('gender') ||
        $errors->first('birthdate') ||
        $errors->first('about')) {
    $profileOpen = true;
} elseif ($errors->first('newPassword') ||
        $errors->first('password') ||
        session('error') !== null) {
    $passwordOpen = true;
}
?>
@extends('layouts.user')

@section('title')
Settings
@endsection

@section('style')
<link type="text/css" rel="stylesheet" href="{{ asset('css/imgareaselect-default.css') }}"  media="screen,projection"/>
@endsection

@section('content')
<div class="row">
    <div class="col s12">
        <ul class="collapsible popout" data-collapsible="accordion">
            <li>
                <div class="collapsible-header green z-depth-1 {{($profileOpen)?'active':''}}">
                    <a class="white-text" style="font-size:18px">
                        <i class="mdi mdi-account"></i>
                        Profile
                    </a>
                </div>
                <div class="collapsible-body" id="profile">
                    <div class="row">
                        <div class="col s10 push-s1">
                            @include('shared.formError')
                            <form action="{{url('settings/updateProfile')}}" method="post">
                                {!! csrf_field() !!}
                                <div class="input-field">
                                    <input type="text" name="name" value="{{Auth::user()->profile->name}}" required/>
                                    <label class="active" for="name">Nama</label>
                                </div>
                                <div class="input-field">
                                    <input type="email" name="email" value="{{Auth::user()->email}}" required/>
                                    <label class="active" for="email">Email</label>
                                </div>
                                <div class="input-field">
                                    <select name="gender">
                                        <option value="1" {{(Auth::user()->profile->gender)?'selected':''}}>Laki - Laki</option>
                                        <option value="0" {{(!Auth::user()->profile->gender)?'selected':''}}>Perempuan</option>
                                    </select>
                                    <label>Jenis Kelamin</label>
                                </div>
                                <div class="input-field">
                                    <input type="date" name="birthdate" class="datepicker" value="{{Auth::user()->profile->birthdate}}" required>
                                    <label class="active" for="birthdate">Tanggal Lahir</label>
                                </div>
                                <div class="input-field">
                                    <div class="input-field">
                                        <textarea name="about" id="about" class="materialize-textarea" length="140">{{Auth::user()->profile->about}}</textarea>
                                        <label class="active" for="about">About Me</label>
                                    </div>
                                </div>
                                <button class="green btn waves-effect waves-light" type="submit" name="submit">
                                    Submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="collapsible-header green z-depth-1">
                    <a class="white-text" style="font-size:18px">
                        <i class="mdi mdi-account-box-outline"></i>
                        Avatar
                    </a>
                </div>
                <div class="collapsible-body" id="avatar">
                    <div class="row" id="preview">
                        <div class="col s3 push-s1" >
                            <img src="{{asset('img/user/'.Auth::user()->id.'.jpg')}}" class="responsive-img">
                        </div>
                        <form id="formAvatar" enctype="multipart/form-data" action="settings/review" method="post">
                            {!! csrf_field() !!}
                            <div class="col s7 push-s1">
                                <div class="file-field input-field">
                                    <div class="green btn waves-effect waves-light" id="inFile">
                                        <span>File</span>
                                        <input id="uploadImage" name="image" type="file" accept=".jpg, .png">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                                <button id="button" class="green btn waves-effect waves-light" id="submit">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li >
                <div class="collapsible-header green z-depth-1 {{($passwordOpen)?'active':''}}">
                    <a class="white-text" style="font-size:18px">
                        <i class="mdi mdi-key"></i>
                        Password
                    </a>
                </div>
                <div class="collapsible-body" id="password">
                    <div class="row">
                        <div class="col s10 push-s1" >
                            @include('shared.formError')
                            @if(null !== session('error'))
                            <div class='red-text'>
                                {{session('error')}}
                            </div>
                            @endif
                            <form action="settings/updatePassword" method="post">
                                {!! csrf_field() !!}
                                <div class="input-field">
                                    <input type="password" name="oldPassword"/>
                                    <label for="oldPassword">Password Lama</label>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="password"/>
                                    <label for="password">Password Baru</label>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="password_confirmation"/>
                                    <label for="password_confirmation">Konfirmasi Password</label>
                                </div>
                                <button id="button" class="green btn waves-effect waves-light" id="submit">
                                    Submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.imgareaselect.pack.js')}}"></script>
<script>
$(document).ready(function () {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 2
    });
    $('select').material_select();
    $('#about').trigger('autoresize');
    $('textarea#about').characterCounter();

    $("#formAvatar").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: "settings/review",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {},
            success: function (data)
            {
                // view uploaded file.
                $("#preview").html(data).fadeIn();
                $("#formAvatar")[0].reset();
            },
            error: function (e) {}
        });
    }));
});

function avatarReady() {
    $("#thumbnail").imgAreaSelect({
        aspectRatio: "1:1",
        onSelectChange: preview
    });
}

function preview(img, selection) {
    var scaleX = 100 / selection.width;
    var scaleY = 100 / selection.height;

    $("#thumbnail + div > img").css({
        width: Math.round(scaleX * 200) + "px",
        height: Math.round(scaleY * 300) + "px",
        marginLeft: "-" + Math.round(scaleX * selection.x1) + "px",
        marginTop: "-" + Math.round(scaleY * selection.y1) + "px"
    });
    $("#x1").val(selection.x1);
    $("#y1").val(selection.y1);
    $("#x2").val(selection.x2);
    $("#y2").val(selection.y2);
    $("#fullw").val($("#thumbnail").width());
    $("#fullh").val($("#thumbnail").height());
}
</script>
@endsection