@extends('layouts.user')

@section('title')
{{$user->profile->name.' Profile'}}
@endsection

@section('style')
<style>
    .tabs .indicator{
        background-color : #4CAF50;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col s12">
        <ul class="tabs">
            <li class="tab col s4">
                <a href="#tabs1" class="green-text">
                    Profile
                </a>
            </li>
            <li class="tab col s4">
                <a href="#tabs2" class="green-text {{(Request::input('pagea')!=null)?'active':''}}">
                    {{'Post('.$totUserPosts.')'}}
                </a>
            </li>
            <li class="tab col s4">
                <a href="#tabs3" class="green-text {{(Request::input('pageb')!=null)?'active':''}}">
                    {{'Post Liked('.$totUserLikedPosts.')'}}
                </a>
            </li>
        </ul>
    </div>
    <div id="tabs1">
        <div class="col s4 push-s4">
            <div class="card">
                <div class="card-image">
                    <img class="materialboxed responsive-img" src="{{asset('img/user/'.$user->id.'.jpg')}}">
                </div>
            </div>
        </div>
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header">
                    <span style="font-size:20px">
                        {{$user->username}}
                    </span>
                    @if($user->role=='admin')
                    <a href="#!" class="tooltipped" data-tooltip="Verified Admin" data-position="right">
                        <i class="mdi mdi-verified"></i>
                    </a>
                    @endif
                </li>
                <li class="collection-item avatar">
                    <img src="{{asset('img/user/'.$user->id.'.jpg')}}" class="circle">
                    <b class="title">Nama</b>
                    <p>{{$user->profile->name}}</p>
                    <?php
                    if (Auth::user()->id != $user->id) {
                        ?>
                        <div class="follow{{$user->id}}">
                            <a href="#!" class="secondary-content waves-effect waves-green btn-flat" onclick="follow('{{$user->id}}')">
                                <?php
                                $follows = Auth::user()->following;
                                $status = 'Follow';
                                foreach ($follows as $follow) {
                                    if ($follow->user_id_foll == $user->id) {
                                        $status = 'Unfollow';
                                    }
                                }
                                echo $status;
                                ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </li>
                <li class="collection-item avatar">
                    <i class="mdi mdi-account circle green"></i>
                    <b class="title">About Me</b>
                    <p>{{($user->profile->about=='')?'Agan ini masih malu untuk menjelaskan dirinya':$user->profile->about}}</p>
                </li>
                <li class="collection-item avatar">
                    <i class="mdi mdi-gender-{{($user->profile->gender)?'male':'female'}} circle green"></i>
                    <b class="title">Jenis Kelamin</b>
                    <p>{{($user->profile->gender)?'Laki - Laki':'Perempuan'}}</p>
                </li>
                <li class="collection-item avatar">
                    <i class="mdi mdi-cake-variant circle green"></i>
                    <b class="title">Tanggal Lahir</b>
                    <p>{{date('d M Y',strtotime($user->profile->birthdate))}}</p>
                </li>
                <li class="collection-item avatar">
                    <i class="mdi mdi-account-multiple circle green"></i>
                    <b class="title">Following</b>
                    <p>{{$user->following->count()}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div id="tabs2" class="col s12">
        <?php
        $totPost = 0;
        $open = false;
        foreach ($userPosts as $post) {
            $doPost = false;
            if ($totPost % 2 == 0) {
                $open = true;
                echo "<div class='row'>";
            }
            ?>

            @include('shared.showPost')

            <?php
            if ($totPost % 2 == 1) {
                $open = false;
                echo "</div>";
            }
            $totPost++;
        }

        if ($open) {
            echo '</div>';
        }
        ?>
        <div class="row center">
            <?php
            ?>
            {!! (new App\MyLib\CustomPagination($userPosts))->render() !!}
        </div>
    </div>
    <div id="tabs3" class="col s12">
        <?php
        $totPost = 0;
        $open = false;
        foreach ($userLikedPosts as $post) {
            $doPost = false;
            if ($totPost % 2 == 0) {
                $open = true;
                echo "<div class='row'>";
            }
            ?>

            @include('shared.showPost')

            <?php
            if ($totPost % 2 == 1) {
                $open = false;
                echo "</div>";
            }
            $totPost++;
        }

        if ($open) {
            echo '</div>';
        }
        ?>
        <div class="row center">
            <?php
            ?>
            {!! (new App\MyLib\CustomPagination($userLikedPosts))->render() !!}
        </div>
    </div>
</div>
@if(Auth::user()->id == $user->id)
<!-- Modal Notifikasi -->
<div id="modalNotif" class="modal">
    <div class="modal-content">
        <h4 class="red-text">From admin</h4>
        <ul class="collection">
            <?php
            $messages = $user->adminMessage;
            $alert = '';
            if ($messages->count() > 0) {
                $alert = '-alert';
                foreach ($messages as $message) {
                    echo '<li class="collection-item">' . $message->message . '</li>';
                }
            } else {
                echo '<li class="collection-item">Tidak ada pesan</li>';
            }
            ?>
        </ul>
    </div>
</div>
<!-- Float button untuk notifikasi pesan admin -->
<div class="fixed-action-btn" style="bottom: 1em; right: 1em;">
    <a class="modal-trigger btn-floating btn-large waves-effect waves-light red" href="#modalNotif">
        <i class="mdi mdi-message{{$alert}}"></i>
    </a>
</div>
@endif
@endsection

@section('script')
<script>
    $(".dropdown-button").dropdown();
    $('ul.tabs').tabs();
    $('.modal-trigger').leanModal();
</script>
@endsection