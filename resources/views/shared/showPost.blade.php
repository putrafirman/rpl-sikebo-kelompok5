<?php
$user_auth = Auth::user();

$push = '';
if ($size == 'med') {
    $size = '6';
    $sizeImg = '2';
} elseif ($size == 'home') {
    $size = '8';
    $sizeImg = '2';
    $push = 'push-m2';
} else {
    $size = '12';
    $sizeImg = '1';
}
$sizeNama = 12 - $sizeImg;

$reportColor = 'grey';
foreach ($user_auth->reported as $report) {
    if ($report->post_id == $post->id) {
        $reportColor = 'red';
    }
}

$likeColor = 'grey';
foreach ($user_auth->postLiked as $liked) {
    if ($liked->post_id == $post->id) {
        $likeColor = 'red';
    }
}
?>
<!-- batas -->

<div class="divider"></div>
  <div class="card">
    <div class="row">
      <div class="col s12 m4 l3"> <!-- Note that "m4 l3" was added -->
        <div class="card-image" style="padding-left: 10px; padding-bottom: 10px; padding-right: 10px; padding-top: 25px;">
        <img class="responsive-img materialboxed" data-caption="{{$post->caption}}" src="{{asset('img/post/'.$post->id.'.jpg')}}">
        </div>
      </div>

      <div class="col s12 m8 l9 purple lighten-5"> <!-- Note that "m8 l9" was added -->
        <div class="card-content">  
            <!-- <div class="card-content"> -->
                <div class="row">
                    <div class="col s12 m6 l3"><p><b>Judul Film</b></p></div>
                    <div class="col s12 m4 l2"><p>{{$post->title}}</p></div>
                </br>
                </div>
                <div class="row">
                    <div class="col s12 m6 l3"><p><b>Penilaian Singkat</b></p></div>
                    <div class="col s12 m4 l9"><p>{{$post->caption}}</p></div>
                </br>
                </div>
                <div class="row">
                    <div class="col s12 m6 l3"><p><b>Deskripsi Film</b> </p></div>
                    <div class="col s12 m4 l9"><p>{{$post->description}}</p></div>
                </br>
                </div>
                <div class="row">
                    <div class="col s12 m6 l3"><p><b>Tahun Rilis</b></p></div>
                    <div class="col s12 m4 l9"><p>{{$post->year}}</p></div>
                </br>
                </div>
                <div class="row">
                    <div class="col s12 m6 l3"><p><b>Trailler Film</b></p></div>
                    <div class="col s12 m4 l9"><p><a href="{{$post->youtube}}">Stream on Youtube</a></p></div>
                </div>
            <!-- </div> -->
            <div class="row valign-wrapper">
                <div class="col s1">
                    <a href="{{url('user/'.$post->user->username)}}">
                        <img src="{{asset('img/user/'.$post->user->id.'.jpg')}}" class="circle responsive-img">
                        
                    </a>
                </div>
                <div class="col s11">
                    <span class="black-text">
                        <a href="{{url('user/'.$post->user->username)}}">{{$post->user->username}}</a>
                        
                    </span>
                </div>
                @if($post->user_id == Auth::user()->id && $size == 12)
                <a href="#!" class="right" style="font-size:18px" onclick="editPost()">
                    <i class="mdi mdi-pencil right"></i>
                </a>
                <a href="#!" class="right" style="font-size:18px" onclick="deletePost()">
                    <i class="mdi mdi-delete right"></i>
                </a>
                @endif
            </div>
            <p style="font-size:9px" class="right">
                {{$post->created_at}}
                @if($post->created_at != $post->updated_at)
                (Edited {{$post->updated_at}})
                @endif
            </p>
        </div>

        <!-- Teal page content

              This content will be:
          9-columns-wide on large screens,
          8-columns-wide on medium screens,
          12-columns-wide on small screens  -->

        <div class="card-action black-text">
            @if($size !== '12')
            <a href="{{url('post/'.$post->id)}}">More</a>
            @else
            <a class="black-text">#{{$post->id}}</a>
            @endif

            <span class="report{{$post->id}}">
                <a href="#!" class="right {{$reportColor}}-text" onclick="report({{$post->id}})" style="font-size:18px">
                    {{$post->reported->count()}}
                    <i class="mdi mdi-flag"></i>
                </a>
            </span>
            <a class="right {{($post->comment->count()>0)?'teal':'grey'}}-text" style="font-size:18px">
                {{$post->comment->count()}}
                <i class="mdi mdi-comment-text-outline"></i>
            </a>
            <span class="like{{$post->id}}">
                <a href="#!" class="right {{$likeColor}}-text" onclick="like({{$post->id}})" style="font-size:18px">
                    {{$post->liked->count()}}
                    <i class="mdi mdi-heart"></i>
                </a>
            </span>
        </div>
    </div>

    </div>
  </div>