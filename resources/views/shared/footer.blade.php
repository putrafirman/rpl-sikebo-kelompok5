<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }
</style>
<footer class="page-footer purple">
    <div class="footer-copyright">
        <div class="container">
            <a href="http://ardi.my.id">©{{date('Y')}} PFNetworks</a>
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>