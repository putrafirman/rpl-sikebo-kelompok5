<div class="col s12 m7">
    <h2 class="header">{{$post->title}}</h2>
    <div class="card horizontal">
      <div class="card-image">
        <img src="{{asset('img/post/'.$post->id.'.jpg')}}">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <div class="row valign-wrapper">
                <div class="col s{{$sizeImg}}">
                    <a href="{{url('user/'.$post->user->username)}}">
                        <img src="{{asset('img/user/'.$post->user->id.'.jpg')}}" class="circle responsive-img">
                    </a>
                </div>
                <div class="col s{{$sizeNama}}">
                    <span class="black-text">
                        <a href="{{url('user/'.$post->user->username)}}">{{$post->user->username}}</a>
                    </span>
                </div>
                @if($post->user_id == Auth::user()->id && $size == 12)
                <a href="#!" class="right" style="font-size:18px" onclick="editPost()">
                    <i class="mdi mdi-pencil right"></i>
                </a>
                <a href="#!" class="right" style="font-size:18px" onclick="deletePost()">
                    <i class="mdi mdi-delete right"></i>
                </a>
                @endif
            </div>
            <div class="caption">
                <p>
                    Judul Film        : {{$post->title}} </br>
                    Penilaian Singkat : {{$post->caption}} </br>
                    Deskripsi Film    : {{$post->description}} </br>
                    Tahun Rilis       : {{$post->year}} </br>
                </p>
            </div>
            <p style="font-size:9px" class="right">
                {{$post->created_at}}
                @if($post->created_at != $post->updated_at)
                (Edited {{$post->updated_at}})
                @endif
            </p>
        </div>
        <div class="card-action black-text">
          @if($size !== '12')
            <a href="{{url('post/'.$post->id)}}">More</a>
            @else
            <a class="black-text">#{{$post->id}}</a>
            @endif

            <span class="report{{$post->id}}">
                <a href="#!" class="right {{$reportColor}}-text" onclick="report({{$post->id}})" style="font-size:18px">
                    {{$post->reported->count()}}
                    <i class="mdi mdi-flag"></i>
                </a>
            </span>
            <a class="right {{($post->comment->count()>0)?'teal':'grey'}}-text" style="font-size:18px">
                {{$post->comment->count()}}
                <i class="mdi mdi-comment-text-outline"></i>
            </a>
            <span class="like{{$post->id}}">
                <a href="#!" class="right {{$likeColor}}-text" onclick="like({{$post->id}})" style="font-size:18px">
                    {{$post->liked->count()}}
                    <i class="mdi mdi-heart"></i>
                </a>
            </span>
        </div>
      </div>
    </div>
</div>
