@if (count($errors)>0)
<div class="error">
    <ul class="red-text">
        <li>
            <strong>
                Terjadi error:
            </strong>
        </li>
        @foreach ($errors->all() as $error)
        <li>
            {{ $error }}
        </li>
        @endforeach
    </ul>
</div>
@endif