<script>
    function like(post_id) {
        var url = "{{url('post/like')}}";
        $.ajax({
            url: url + '/' + post_id,
            type: 'get',
            success: function (data) {
                $(".like" + post_id).html(data);
            }
        });
    }

    function report(post_id) {
        var url = "{{url('post/report')}}";
        $.ajax({
            url: url + '/' + post_id,
            type: 'get',
            success: function (data) {
                $(".report" + post_id).html(data);
            }
        });
    }

    function likeComment(comment_id) {
        var url = "{{url('comment/like')}}";
        $.ajax({
            url: url + '/' + comment_id,
            type: 'get',
            success: function (data) {
                $(".likeComment" + comment_id).html(data);
            }
        });
    }

    function follow(id_user) {
        var url = "{{url('user/follow')}}";
        $.ajax({
            url: url + '/' + id_user,
            type: 'get',
            success: function (data) {
                $(".follow" + id_user).html(data);
            }
        });
    }

    /*@if(isset($post))*/
    function editPost() {
        var url = "{{url('post/'.$post->id.'/editCaption')}}";
        var form = '<form action="' + url + '" method="post" class="form-input">' +
                '{!! csrf_field() !!}' +
                '<div class="input-field">' +
                '<textarea name="caption" id="caption" class="materialize-textarea" length="140">{{$post->caption}}</textarea>' +
                '<label class="active" for="caption">Caption</label>' +
                '</div>' +
                '<button class="green btn waves-effect waves-light" id="submit">' +
                'Submit' +
                '</button>' +
                '</form>';
        $('.caption').html(form);
        $('#caption').trigger('autoresize');
        $('textarea#caption').characterCounter();
    }

    function deletePost() {
        var url = "{{url('post/'.$post->id.'/delete')}}";
        if (confirm("Yakin ingin menghapus post ini?")) {
            window.location.replace(url);
        }
    }
    /*@endif*/

    /*@if(isset($comment))*/
    function editComment(comment_id) {
        var url = "{{url('comment/edit/')}}";
        var form = '<form action="' + url + '/' + comment_id + '" method="post">' +
                '{!! csrf_field() !!}' +
                '<div class="input-field">' +
                '<textarea name="comment" id="caption" class="materialize-textarea" length="140"></textarea>' +
                '<label for="comment" class="active">Caption</label>' +
                '</div>' +
                '<button class="green btn waves-effect waves-light" id="submit">' +
                'Submit' +
                '</button>' +
                '</form>';
        $('.comment' + comment_id).html(form);
        $('#comment').trigger('autoresize');
        $('textarea#comment').characterCounter();
    }

    function deleteComment(comment_id) {
        var url = "{{url('comment/delete/')}}";
        if (confirm("Yakin ingin menghapus comment ini?")) {
            window.location.replace(url + '/' + comment_id);
        }
    }
    /*@endif*/

    /*@if (session('status'))*/
    Materialize.toast('{{session('status')}}', 5000);
    /*@endif*/
</script>