<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }
</style>
<footer class="page-footer light-blue darken-3">
    <div class="footer-copyright">
        <div class="container">
            <a href="http://ardi.my.id">©{{date('Y')}} PFNetworks</a>
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>