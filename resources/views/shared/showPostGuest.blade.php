<?php

$push = '';
if ($size == 'med') {
    $size = '6';
    $sizeImg = '2';
} elseif ($size == 'home') {
    $size = '8';
    $sizeImg = '2';
    $push = 'push-m2';
} else {
    $size = '12';
    $sizeImg = '1';
}
$sizeNama = 12 - $sizeImg;

?>
<div class="col s12 m{{$size.' '.$push}}">
    <div class="card" >
        <div class="card-image">
            <img class="responsive-img materialboxed" data-caption="{{$post->caption}}" src="{{asset('img/post/'.$post->id.'.jpg')}}">
        </div>
        <div class="card-content">
            <div class="row valign-wrapper">
                <div class="col s{{$sizeImg}}">
                    
                        <img src="{{asset('img/user/'.$post->user->id.'.jpg')}}" class="circle responsive-img">
                    
                </div>
                <div class="col s{{$sizeNama}}">
                    <span class="black-text">
                        {{$post->user->username}}
                    </span>
                </div>
            </div>
            <div class="caption">
                <p>
                    {{$post->caption}}
                </p>
            </div>
            <p style="font-size:9px" class="right">
                {{$post->created_at}}
                @if($post->created_at != $post->updated_at)
                (Edited {{$post->updated_at}})
                @endif
            </p>
        </div>
        <div class="card-action black-text">
            @if($size !== '12')
            <a href="{{url('post/'.$post->id)}}">More</a>
            @else
            <a class="black-text">#{{$post->id}}</a>
            @endif

            <a class="right {{($post->comment->count()>0)?'teal':'grey'}}-text" style="font-size:18px">
                {{$post->comment->count()}}
                <i class="mdi mdi-comment-text-outline"></i>
            </a>

        </div>
    </div>
</div>