@extends('layouts.user')

@section('title')
Home
@endsection

@section('content')
<?php
foreach ($posts as $post) {
    echo "<div class='row'>";
    ?>

    @include('shared.showPost')

    <?php
    echo '</div>';
}
?>
<!-- Modal Upload -->
<div id="modalUpload" class="modal">
    <form enctype="multipart/form-data" action="{{url('post')}}" method="post">
        {!! csrf_field() !!}
        <div class="modal-content">
            <h4>New Post</h4>
            @include('shared.formError')
            <div class="file-field input-field">
                <div class="green btn waves-effect waves-light" id="inFile">
                    <span>File</span>
                    <input name="image" type="file" accept=".jpg, .png">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
            <div class="input-field">
                <textarea name="title" id="title" class="materialize-textarea" length="100"></textarea>
                <label for="title">Judul</label>
            </div>
            <div class="input-field">
                <textarea name="caption" id="caption" class="materialize-textarea" length="255"></textarea>
                <label for="caption">Penilaian Singkat</label>
            </div>
            <div class="input-field">
                <textarea name="description" id="description" class="materialize-textarea" length="255"></textarea>
                <label for="description">Deskripsi</label>
            </div>
            <div class="input-field">
                <textarea name="year" id="year" class="materialize-textarea" length="5"></textarea>
                <label for="year">Tahun</label>
            </div>
            <div class="input-field">
                <textarea name="youtube" id="youtube" class="materialize-textarea" length="255"></textarea>
                <label for="youtube">Link Trailer</label>
            </div>
        </div>
        <div class="modal-footer">
            <button class="green btn waves-effect waves-light" id="submit">
                Submit
            </button>
        </div>
    </form>
</div>
<!-- Float Button-->
<div class="fixed-action-btn" style="bottom: 1em; right: 1em;">
    <a class="modal-trigger btn-floating btn-large waves-effect waves-light red" href="#modalUpload">
        <i class="mdi mdi-plus"></i>
    </a>
</div>
<div class="row center">
    {!! (new App\MyLib\CustomPagination($posts))->render() !!}
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $(".dropdown-button").dropdown();
        $('.modal-trigger').leanModal();
        $('#caption').val('');
        $('#caption').trigger('autoresize');
        $('textarea#caption').characterCounter();
        
        $('.materialboxed').materialbox();
        /*@if (count($errors) > 0)*/
        $('#modalUpload').openModal();
        /*@endif*/
    });

    var socket = io.connect('http://localhost:3000');
    
    /*@if (session('status')=='Berhasil post!')*/
    socket.emit('post', 'do it');
    /*@endif*/

    socket.on('post', function (msg) {
        var url = "{{url('/latestpost')}}";
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                $(".container").prepend($('<div class="row">').html(data));
            }
        });
    });
</script>
@endsection
