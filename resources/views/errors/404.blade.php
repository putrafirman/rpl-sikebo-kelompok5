<!DOCTYPE html>
@include('shared.easteregg')
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>
            404 Page Not Found
        </title>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>
    </head>
    <body class="green">
        <main>
            <div class="row" style="margin-top: 4em">
                <div class="col s10 push-s1 m6 push-m3 l4 push-l4">
                    <div class="card white z-depth-2">
                        <div class="card-content black-text">
                            <p class="center" style="font-size:100px; color:#A2A2A2;">404</p>
                            <h4 class="center" style="color:#525252">Page not found</h4>
                        </div>
                        <div class="card-action">
                            <a class="green btn waves-effect waves-light" type="submit" href="{{url('/')}}" style="width:100%">
                                Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
