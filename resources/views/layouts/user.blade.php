<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>
            @yield('title')
        </title>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialdesignicons.min.css') }}"  media="screen,projection"/>
        @yield('style')
    </head>
    <body class="{{(Auth::guest())?'light-blue lighten-4':'purple lighten-4'}}">
        @if (Auth::user())
        <!-- Dropdown Navbar Content -->
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="{{url('user/'.Auth::user()->username)}}">Profile</a></li>
            <li><a href="{{url('settings')}}">Setting</a></li>
            <li><a href="{{url('logout')}}">Logout</a></li>
        </ul>
        <!-- Navbar -->
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper purple">
                    <a href="{{url('')}}" class="brand-logo">Logo</a>
                    <ul id="nav-mobile" class="right">
                        <li>
                            <a href="{{url('')}}">Home</a>
                        </li>
                        <li>
                            <a class="dropdown-button" href="#!" data-activates="dropdown1">
                                {{Auth::user()->username}}
                                <i class="mdi mdi-menu-down right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        @else
        <!-- Dropdown Navbar Content -->
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="{{url('register')}}">Register</a></li>
            <li><a href="{{url('login')}}">Login</a></li>
        </ul>
        <!-- Navbar -->
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper light-blue darken-3">
                    <a href="{{url('')}}" class="brand-logo">Logo</a>
                    <ul id="nav-mobile" class="right">
                        <li>
                            <a href="{{url('')}}">Home</a>
                        </li>
                        <li>
                            <a class="dropdown-button" href="#!" data-activates="dropdown1">
                                Guest
                                <i class="mdi mdi-menu-down right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        @endif
        <main>
            @if(Auth::user())
            <div class="container">
                @yield('content')
            </div>
            @else
            @yield('content')
            @endif
            <div class="container">
            @yield('content.guest')
            </div>
        </main>
        @if(Auth::user())
        @include('shared.footer')
        @else
        @include('shared.footerGuest')
        @endif

        <script type="text/javascript" src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
        <script src="http://localhost:3000/socket.io/socket.io.js"></script>
        @include('shared.easteregg')
        @include('shared.doJs')
        @yield('script')
    </body>
</html>
