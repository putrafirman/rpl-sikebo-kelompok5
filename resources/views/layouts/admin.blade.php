<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>
            @yield('title')
        </title>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialdesignicons.min.css') }}"  media="screen,projection"/>
        <style>
            header, main, footer {
                padding-left: 240px;
            }

            @media only screen and (max-width : 992px) {
                header, main, footer {
                    padding-left: 0;
                }
            }
        </style>
        @yield('style')
    </head>
    <body>
        <nav class="green">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo right">@yield('nav-title')</a>
                <ul id="slide-out" class="side-nav fixed">
                    <li class="green">
                        <a class="white-text">{{Auth::user()->username}}</a>
                    </li>
                    <li><a href="{{url('admin')}}">Dashboard</a></li>
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            <li>
                                <a class="collapsible-header">CRUD</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="{{url('admin/user')}}">Users</a></li>
                                        <li><a href="{{url('admin/post')}}">Posts</a></li>
                                        <li><a href="{{url('admin/comment')}}">Comments</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{url('home')}}">Site</a></li>
                </ul>
                <a href="#!" data-activates="slide-out" class="button-collapse" style="font-size:18px">
                    <i class="mdi mdi-menu"></i>
                </a>
            </div>
        </nav>
        <main>
            <div class="container" style="margin-top:1em">
                @yield('content')
            </div>
        </main>

        <script type="text/javascript" src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
        @include('shared.easteregg')
        @yield('script')
        <script>
$(document).ready(function () {
    $(".button-collapse").sideNav();
});
        </script>
    </body>
</html>
