@extends('layouts.admin')

@section('title')
CRUD Comments
@endsection

@section('style')
<style>
    table.striped>tbody>tr:nth-child(odd) {
        background-color: #FFF;
    }

    table.striped>tbody>tr:nth-child(even) {
        background-color: #C8E6C9;
    }
</style>
@endsection

@section('nav-title')
CRUD Comments
@endsection

@section('content')
<div class="row">
    <table id="table" class="tablesorter striped centered">
        <thead class="green white-text">
            <tr>
                <th data-field="id">id</th>
                <th data-field="post_id">post_id</th>
                <th data-field="poster">poster</th>
                <th data-field="comment">Comment</th>
                <th data-field="like">Likes</th>
                <th data-field="created_at">Created at</th>
                <th data-field="op" colspan="3">Op</th>
            </tr>
        </thead>
        <tbody>
            @foreach($comments as $comment)
            @if(isset($_GET['post']) && $_GET['post']==$comment->post->id)
            <tr class='yellow lighten-4'>
                @else
            <tr>
                @endif
                <td>{{$comment->id}}</td>
                <td>{{$comment->post->id}}</td>
                <td>{{$comment->user->username}}</td>
                <td>{{$comment->comment}}</td>
                <td>{{$comment->liked->count()}}</td>
                <td>{{date('d M Y',strtotime($comment->created_at))}}</td>
                <td>
                    <a href="{{url('post/'.$comment->post->id)}}">
                        Post
                    </a>
                </td>
                <td>
                    <a href="#modalForm" class="modal-trigger" onclick="editComment('{{$comment->id}}')">
                        Edit
                    </a>
                </td>
                <td>
                    <a href="#!" onclick="deleteComment('{{$comment->id}}')">
                        Delete
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <a href="#modalForm" class="btn waves-effect waves-light green modal-trigger" onclick="createComm()" style="width:100%">
        Create New
    </a>
    @include('shared.formError')
</div>

<div id="modalForm" class="modal"></div>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.tablesorter.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#table').tablesorter();
        $('.modal-trigger').leanModal();
        /*@if (session('status'))*/
        Materialize.toast('{{session('status')}}', 5000);
        /*@endif*/
    });
    function deleteComment(id){
        var url = "{{url('admin/comment/delete/')}}/" + id;
        if (confirm("Yakin ingin menghapus comment ini?")) {
        window.location.replace(url);
        }
    }

    function createComm(){
        var url = "{{url('admin/comment/show')}}";
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
            $("#modalForm").html(data);
            $('#comment').trigger('autoresize');
            $('textarea#comment').characterCounter();
            $('select').material_select();
            }
        });
    }

    function editComment(id){
        var url = "{{url('admin/comment/show')}}/" + id;
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
            $("#modalForm").html(data);
            $('#comment').trigger('autoresize');
            $('textarea#comment').characterCounter();
            }
        });
    }
</script>
@endsection