@extends('layouts.admin')

@section('title')
CRUD Posts
@endsection

@section('style')
<style>
    table.striped>tbody>tr:nth-child(odd) {
        background-color: #FFF;
    }

    table.striped>tbody>tr:nth-child(even) {
        background-color: #C8E6C9;
    }
</style>
@endsection

@section('nav-title')
CRUD Users
@endsection

@section('content')
<div class="row">
    <table id="table" class="tablesorter striped centered">
        <thead class="green white-text">
            <tr>
                <th data-field="id">id</th>
                <th data-field="poster">Poster</th>
                <th data-field="caption">Caption</th>
                <th data-field="like">Likes</th>
                <th data-field="report">Reports</th>
                <th data-field="report">Comments</th>
                <th data-field="created_at">Created at</th>
                <th data-field="op" colspan="4">Op</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{$post->id}}</td>
                <td>{{$post->user->username}}</td>
                <td>{{$post->caption}}</td>
                <td>{{$post->liked->count()}}</td>
                <td>{{$post->reported->count()}}</td>
                <td>{{$post->comment->count()}}</td>
                <td>{{date('d M Y',strtotime($post->created_at))}}</td>
                <td>
                    <a href="{{url('post/'.$post->id)}}">
                        Post
                    </a>
                </td>
                <td>
                    <a href="{{url('admin/comment?post='.$post->id)}}">
                        Comments
                    </a>
                </td>
                <td>
                    <a href="#modalForm" class="modal-trigger" onclick="editPost('{{$post->id}}')">
                        Edit
                    </a>
                </td>
                <td>
                    <a href="#!" onclick="deletePost('{{$post->id}}')">
                        Delete
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <a href="#modalForm" class="btn waves-effect waves-light green modal-trigger" onclick="createPost()" style="width:100%">
        Create New
    </a>
    @include('shared.formError')
</div>

<div id="modalForm" class="modal">
    
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.tablesorter.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#table').tablesorter();
        $('.modal-trigger').leanModal();
        /*@if (session('status'))*/
        Materialize.toast('{{session('status')}}', 5000);
        /*@endif*/
    });
    
    function deletePost(id){
        var url = "{{url('admin/post/delete/')}}/"+id;
        if (confirm("Yakin ingin menghapus post ini?")) {
            window.location.replace(url);
        }
    }
    
    function createPost(){
        var url = "{{url('admin/post/show')}}";
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                $("#modalForm").html(data);
                $('#caption').trigger('autoresize');
                $('textarea#caption').characterCounter();
            }
        });
    }
    
    function editPost(id){
        var url = "{{url('admin/post/show')}}/"+id;
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                $("#modalForm").html(data);
                $('#caption').trigger('autoresize');
                $('textarea#caption').characterCounter();
            }
        });
    }
</script>
@endsection