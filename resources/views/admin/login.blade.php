@extends('layouts.user')

@section('title')
Admin Login
@endsection

@section('content')
<div class="row" style="margin-top: 4em">
    <div class="col s10 push-s1 m6 push-m3 l4 push-l4">
        <div class="card white z-depth-2">
            {{ Form::open(array('url' =>'admin/login', 'class' =>'form-input')) }}
            <div class="card-content black-text">
                <span class="card-title">
                    Admin Login
                </span>
                <div class="divider" style="margin-bottom:0.5em">
                </div>
                @include('shared.formError')
                @if(session('gagal'))
                {{session('gagal')}}
                @endif
                <div class="input-field">
                    {{ Form::text('username') }}
                    {{ Form::label('username', 'Username') }}
                </div>
                <div class="input-field">
                    {{ Form::password('password') }}
                    {{ Form::label('password', 'Password') }}
                </div>
            </div>
            <div class="card-action">
                <button class="green btn waves-effect waves-light">
                    Login
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection