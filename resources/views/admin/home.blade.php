@extends('layouts.admin')

@section('title')
Admin
@endsection

@section('nav-title')
Admin Panel
@endsection

@section('content')
<div class="row">
    <div class="col s12 m6">
        <div class="card red lighten-1 white-text">
            <div class="card-content">
                <span class="card-title">
                    Users
                </span>
                <div class="divider"></div>
                <p>Total Users: {{$users->where('role','user')->count()}}</p>
                <p>Total Admins: {{$users->where('role','admin')->count()}}</p>
                @if(isset($mostFollowedUser))
                <a href="{{url('user/id/'.$mostFollowedUser->id)}}" class="truncate white-text">
                    Most Followed User:
                    ({{$mostFollowedUser->followed->count()}})
                    {{$mostFollowedUser->username}}
                </a>
                @endif
            </div>
            <div class="card-action red darken-1">

            </div>
        </div>
    </div>
    <div class="col s12 m6">
        <div class="card yellow darken-2 white-text">
            <div class="card-content">
                <span class="card-title">
                    Posts
                </span>
                <div class="divider"></div>
                <p>Total Posts: {{$posts->count()}}</p>
                @if(isset($mostLikedPost))
                <a href="{{url('post/'.$mostLikedPost->id)}}" class="truncate white-text">
                    Most Liked Post:
                    ({{$mostLikedPost->liked->count()}})
                    {{$mostLikedPost->caption}}
                </a>
                @endif
                @if(isset($mostReportedPost))
                <a href="{{url('post/'.$mostReportedPost->id)}}" class="truncate white-text">
                    Most Reported Post:
                    ({{$mostReportedPost->reported->count()}})
                    {{$mostReportedPost->caption}}
                </a>
                @endif
            </div>
            <div class="card-action yellow darken-3">

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m6">
        <div class="card green white-text">
            <div class="card-content">
                <span class="card-title">
                    Comments
                </span>
                <div class="divider"></div>
                <p>Total Comments: {{$comments->count()}}</p>
                @if(isset($mostLikedComment))
                <a href="{{url('post/'.$mostLikedComment->post_id.'#comments')}}" class="truncate white-text">
                    Most Liked Comment:
                    ({{$mostLikedComment->liked->count()}})
                    {{$mostLikedComment->comment}}
                </a>
                @endif
            </div>
            <div class="card-action green darken-2">

            </div>
        </div>
    </div>
    <div class="col s12 m6">
        <div class="card blue white-text">
            <div class="card-content">
                <span class="card-title">
                    Most Active Users
                </span>
                <div class="divider"></div>
                @if(isset($topPoster))
                <a href="{{url('user/id/'.$topPoster->id)}}" class="truncate white-text">
                    Top Poster:
                    ({{$topPoster->post->count()}})
                    {{$topPoster->username}}
                </a>
                @endif
                @if(isset($topCommenter))
                <a href="{{url('user/id/'.$topCommenter->id)}}" class="truncate white-text">
                    Top Commenter:
                    ({{$topCommenter->comment->count()}})
                    {{$topCommenter->username}}
                </a>
                @endif
            </div>
            <div class="card-action blue darken-2">

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">
                    Admin Messages
                </span>
                <div class="divider"></div>
                <ul class="collection">
                    @if($adminMessages->count()>0)
                    @foreach($adminMessages as $message)
                    <li class="collection-item avatar">
                        <img src="{{asset('img/user/'.$message->sender->id.'.jpg')}}" class="circle">
                        <span class="title">Dari: {{$message->sender->username}}</span>
                        <p>Untuk: {{$message->receiver->username}}<br>
                            {{$message->message}}
                        </p>
                        <a href="#!" class="secondary-content" style="font-size:18px" onclick="deleteMessage({{$message->id}})">
                            <i class="mdi mdi-delete"></i>
                        </a>
                    </li>
                    @endforeach
                    @else
                    <li class="collection-item">Tidak ada pesan</li>
                    @endif
                </ul>
            </div>
            <div class="card-action darken-1">
                @include('shared.formError')
                <form action="{{url('message/send')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="input-field">
                        <textarea name="message" id="message" class="materialize-textarea" length="140"></textarea>
                        <label for="message">Message</label>
                    </div>
                    <div class="input-field">
                        <select name="receiver">
                            @foreach($users as $user)
                            <option value="{{$user->id}}">
                                {{$user->username}}
                            </option>
                            @endforeach
                        </select>
                        <label for="receiver">Penerima</label>
                    </div>
                    <button class="btn waves-effect waves-light green">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
    $('#message').val('');
    $('#message').trigger('autoresize');
    $('textarea#message').characterCounter();
    $('.materialboxed').materialbox();
    $('select').material_select();
    /*@if (session('status'))*/
    Materialize.toast('{{session('status')}}', 5000);
    /*@endif*/
    });
    function deleteMessage(message_id) {
    var url = "{{url('message/delete')}}/" + message_id;
    if (confirm("Yakin ingin menghapus pesan ini?")) {
    window.location.replace(url);
    }
    }
</script>
@endsection