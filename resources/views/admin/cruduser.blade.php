@extends('layouts.admin')

@section('title')
CRUD Users
@endsection

@section('style')
<style>
    table.striped>tbody>tr:nth-child(odd) {
        background-color: #FFF;
    }
    table.striped>tbody>tr:nth-child(even) {
        background-color: #C8E6C9;
    }
</style>
@endsection

@section('nav-title')
CRUD Users
@endsection

@section('content')
<div class="row">
    <table id="table" class="tablesorter striped centered">
        <thead class="green white-text">
            <tr>
                <th data-field="id">id</th>
                <th data-field="username">Username</th>
                <th data-field="role">Role</th>
                <th data-field="email">Email</th>
                <th data-field="name">Name</th>
                <th data-field="birthdate">Birthdate</th>
                <th data-field="created_at">Created at</th>
                <th data-field="op" colspan="3">Op</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->username}}</td>
                <td>{{$user->role}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->profile->name}}</td>
                <td>{{date('d M Y',strtotime($user->profile->birthdate))}}</td>
                <td>{{date('d M Y',strtotime($user->profile->created_at))}}</td>
                @if($user->id!==1)
                <td>
                    <a href="{{url('user/id/'.$user->id)}}">
                        Profile
                    </a>
                </td>
                <td>
                    <a href="#modalForm" class="modal-trigger" onclick="editUser('{{$user->id}}')">
                        Edit
                    </a>
                </td>
                <td>
                    <a href="#!" onclick="deleteUser('{{$user->id}}')">
                        Delete
                    </a>
                </td>
                @else
                <td colspan="3">
                    <a href="{{url('user/id/'.$user->id)}}">
                        Profile
                    </a>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <a href="#modalForm" class="btn waves-effect waves-light green modal-trigger" onclick="createUser()" style="width:100%">
        Create New
    </a>
    @include('shared.formError')
</div>

<div id="modalForm" class="modal">
    
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.tablesorter.min.js')}}"></script>
<script>
    $(document).ready(function(){
       $('#table').tablesorter();
        $('.modal-trigger').leanModal();
       /*@if (session('status'))*/
        Materialize.toast('{{session('status')}}', 5000);
        /*@endif*/
    });
    
    function deleteUser(id){
        var url = "{{url('admin/user/delete/')}}/"+id;
        if (confirm("Yakin ingin menghapus user ini?")) {
            window.location.replace(url);
        }
    }
    
    function createUser(){
        var url = "{{url('admin/user/show')}}";
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                $("#modalForm").html(data);
                $('#about').trigger('autoresize');
                $('textarea#about').characterCounter();
                $('.datepicker').pickadate({
                    selectMonths: true,
                    selectYears: 2
                });
                $('select').material_select();
            }
        });
    }
    
    function editUser(id){
        var url = "{{url('admin/user/show')}}/"+id;
        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                $("#modalForm").html(data);
                $('#about').trigger('autoresize');
                $('textarea#about').characterCounter();
                $('.datepicker').pickadate({
                    selectMonths: true,
                    selectYears: 2
                });
                $('select').material_select();
            }
        });
    }
</script>
@endsection